import React from 'react';
import ReactDOM from 'react-dom';
import Bibliotheque from './Bibliotheque'
import 'bootstrap/dist/css/bootstrap.min.css';


ReactDOM.render(
  <Bibliotheque />,
  document.getElementById('root')
);
