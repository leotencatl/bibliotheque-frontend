import React, { Component } from 'react';
import axios from 'axios';
import { Alert, Button, Col, Form, FormGroup, Label, Input } from 'reactstrap';

class BookForm extends Component {
  

  constructor(props){
    super(props);
    this.state = {
      error : "",
      book: this.props.book
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    let book = this.state.book;
    book[event.target.name] = event.target.value;
    this.setState({
      error: this.state.error,
      book: book
    });
  }

  componentDidUpdate(){
    if(this.state.book.id !== this.props.book.id){
        this.setState({book: this.props.book});
      }
  }

  async handleSubmit(event){
    event.preventDefault();
    const book = this.state.book;
    let method = "POST";
    let url = "/api/book/";
    if(book.id){
      method = "PUT";
      url += book.id
    }
    try{
      await axios({
        method: method,
        url: url,
        headers: { 'content-type': 'application/json' },
        auth: {
            username: 'user',
            password: 'pass'
        },
        data: JSON.stringify(book)
      });
      const error = await this.props.refresh();
      this.setState({error: error, book: this.props.book});
    } catch(error){
      this.setState({error});
    }
  }

  render(){
    const book = this.state.book;
    const header = <h4>{book.id ? "Modifier un livre" : "Ajouter un livre"}</h4>;

    if(this.state.error){
      return (
        <Alert color="danger">
          Un erreur est survenue lors de la sauvegarde du livre : {this.state.error}
        </Alert>
        );
    }

    return(
      <div>
        {header}
        <Form>
          <FormGroup row>
            <Label for="title" sm={1}>Titre</Label>
            <Col sm={6}>
              <Input type="text" name="title" id="title" onChange={this.handleChange} value={book.title} />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="title" sm={1}>Auteur</Label>
            <Col sm={6}>
              <Input type="text" name="author" id="author" onChange={this.handleChange} value={book.author} />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="title" sm={1}>ISBN</Label>
            <Col sm={6}>
              <Input type="text" name="isbn" id="isbn" onChange={this.handleChange} value={book.isbn} />
            </Col>
          </FormGroup>
          <Button color="success" size="sm" onClick={this.handleSubmit}>Sauvegarder</Button>
        </Form>
      </div>
    );
  }
}

export default BookForm;