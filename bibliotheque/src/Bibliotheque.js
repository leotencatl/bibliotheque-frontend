import React, { Component } from 'react';
import axios from 'axios';
import { Container } from 'reactstrap';
import BookTable from './BookTable'
import BookForm from './BookForm'


class Bibliotheque extends Component {
  emptyBook = {
    id: null,
    title: "",
    author: "",
    isbn: ""
  };

  constructor(props){
    super(props);
    this.state = {
      books: [],
      bookInForm: Object.assign({}, this.emptyBook)
    }

  }

  
  async refreshData(){
    let error;
    try{
        const result = await axios.get('/api/book/', {
        auth: {
          username: 'user',
          password: 'pass'
          }
      });
        this.setState({
          books: result.data.content,
          bookInForm: Object.assign({}, this.emptyBook)
        });

      } catch(error) {

      }
      return error;
  }

  editBook(book){
    this.setState({
          books: this.state.books,
          bookInForm: Object.assign({}, book)
        });
  }

  render(){
    return (
      <Container>

        <h3>Bibliothèque José Guadalupe Posada</h3>
        <BookForm
          refresh={() => this.refreshData()}
          book={this.state.bookInForm}
        />
        <br />
        <BookTable 
          refresh={() => this.refreshData()}
          editBook={(book) => this.editBook(book)}
          books={this.state.books}
        />
      </Container>
      );
  }
}
export default Bibliotheque;
