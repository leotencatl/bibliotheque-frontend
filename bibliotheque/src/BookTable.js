import React, { Component } from 'react';
import axios from 'axios';
import { Alert,
         ButtonGroup,
         Button,
         Table } from 'reactstrap';


class BookTable extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      error: null
    };
  }

  async componentDidMount() {
    this.setState({isLoading: true});
    const error  = await this.props.refresh();
    this.setState({isLoading: false, error});
  }

  async handleDelete(id){
    let error;
    try{
      await axios.delete('/api/book/' + id, {
        auth: {
          username: 'user',
          password: 'pass'
          }
      });
    } catch(error){
      this.setState({error});
    }
    if(!error){
      const error  = await this.props.refresh();
      this.setState({isLoading: false, error});
    }
  }

  render(){
    const {isLoading, error} = this.state;
    if(error){
      return (
        <Alert color="danger">
          Un erreur est survenue lors de la récupération des livres : {error}
        </Alert>
        );
    }

    if(isLoading){
      return <p>Loading...</p>;
    }

    return(
      <div size="sm">
            <h4>Livres</h4>
            <Table responsive striped bordered condensed="true" hover>
              <tbody>
                <tr>
                  <th>Titre</th>
                  <th>Auteur</th>
                  <th>ISBN</th>
                  <th></th>
                </tr>
              {this.props.books
                .sort((a,b) => a.id - b.id)
                .map(book => 
                <tr key={book.id}>
                  <td>{book.title}</td>
                  <td>{book.author}</td>
                  <td>{book.isbn}</td>
                  <td>
                    <ButtonGroup>
                      <Button color="warning" size="sm" onClick={() => this.props.editBook(book)}>Modifier</Button>
                      <Button color="danger" size="sm" onClick={() => this.handleDelete(book.id)}>Supprimer</Button>
                    </ButtonGroup>
                  </td>
                </tr>
              )}
              </tbody>
            </Table>
      </div>
    );
  }
}

export default BookTable;